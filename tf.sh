#!/bin/sh
export PASSWORD_STORE_DIR="$HOME/.password-store"

ORIGINAL_DIR=$(pwd)

until [ "$PROJECT_NAME" != '' ]; do
    DIR=$(pwd)

    if [ "$DIR" = '/' ]; then
        PROJECT_NAME=$(basename "$ORIGINAL_DIR")
    elif [ -f .terraform-project ]; then
        PROJECT_NAME=$(cat .terraform-project | head -n 1 | sed "s/\n//")
    elif [ -d .git -o -f .git ]; then
        PROJECT_NAME=$(basename "$DIR")
    else
        cd ..
    fi
done

cd "$ORIGINAL_DIR"

VAULT_ID=${VAULT_ID:-prod}
PROJECT="${PROJECT_OVERRIDE:-$PROJECT_NAME}"
if [ "$VAULT_ID" != "" -a -f "${PASSWORD_STORE_DIR}/terraform/${PROJECT}/${VAULT_ID}.gpg" ]; then
    eval "$(pass show "terraform/$PROJECT/${VAULT_ID}")"
elif [ -f "${PASSWORD_STORE_DIR}/terraform/${PROJECT}.gpg" ]; then
    eval "$(pass show "terraform/$PROJECT")"
else
    exit 1
fi

terraform "$@"
