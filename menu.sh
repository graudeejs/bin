#!/bin/sh

# Generic menu
# No args

HOSTNAME="`hostname`"

[ -f "$XDG_CONFIG_HOME/wmscripts/config" ] && . "$XDG_CONFIG_HOME/wmscripts/config"

[ "$WMSCRIPTS_EXEC_PWD" ] && cd "$WMSCRIPTS_EXEC_PWD"

app() {
    case $# in
    1)
        which $1 > /dev/null && echo $1
        ;;
    2)
        which $1 > /dev/null && echo $2
        ;;
    esac
}

appon() {
    pgrep $1 >/dev/null && app $*
}

appoff() {
    pgrep $1 >/dev/null || app $*
}


menu_config() { # {{{1
    selection=`{
        app "$WM"
        app 'dmenu'
        app 'xmobar'
        echo 'wmscripts/config'
        echo 'Xdefaults'
        echo 'shrc'
        echo 'xinitrc'
        echo 'doc menu'
        app  'xscreensaver-demo'    'screensaver'
        app  'switch2'              'GTK theme'
        app  'qtconfig-qt4'         'QT4 theme'
        app  'qtconfig'             'QT3 theme'
        app  'nvidia-settings'      'nVidia'
        echo 'monitor off in 5min'
        echo 'monitor off in 15min'
        echo 'monitor off in 20min'
        app  'fvwm'
        app  'xmonad'
        app  'xfce4-settings-manager'
        [ "$WM" = 'fvwm' ] && echo 'restart fvwm'
        app 'xmonad'                'ch 2 xmonad'
        app 'fvwm'                  'ch 2 fvwm'
    } | $DMENU`

    case $selection in
    'fvwm' )                    exec $MY_EDITOR ~/.fvwm/config;;
    'xmonad' )                  exec $MY_EDITOR ~/.xmonad/xmonad.hs ;;
    'dmenu' )                   exec $MY_EDITOR ~/bin/menu.sh ;;
    'xinitrc' )                 exec $MY_EDITOR ~/.xinitrc ;;
    'Xdefaults' )               exec $MY_EDITOR ~/.Xdefaults; exec xrdb ~/.Xdefaults;;
    'shrc' )                    exec $MY_EDITOR ~/.shrc ;;
    'screensaver' )             exec nice -n 10 xscreensaver-demo ;;
    'monitor off in 5min' )     exec xset dpms 300 ;;
    'monitor off in 15min' )    exec xset dpms 900 ;;
    'monitor off in 20min' )    exec xset dpms 1200 ;;
    'GTK theme' )               exec switch2 ;;
    'QT4 theme' )               exec qtconfig-qt4 ;;
    'QT3 theme' )               exec qtconfig ;;
    'nVidia' )                  exec nvidia-settings ;;
    'restart fvwm' )            exec FvwmCommand restart ;;
    'ch 2 fvwm' )               exec sed -I.bak -e 's/^export WM=.*/export WM=fvwm/' ~/src/dot.files/dot.xinitrc ;;
    esac
    exit
} # 1}}}

# MAIN MENU {{{1
selection=`{
    echo 'browser'
    echo 'editor'
    echo 'terminal'
    app 'firefox'
    app 'firefox'       'firefox (private)'
    app 'firefox'       'firefox (Evitux)'
    app 'firefox'       'firefox profiles'
    app 'audacious'
    app 'chrome'
    app 'chrome'        'chrome (private)'
    app 'xombrero'
    app 'mmex'          'moneymanagerex'
    app 'urxvt'
    app 'vim'
    app 'workbench'     'sqlworkbench'
    app 'sqlworkbench'  'sqlworkbench'
    app 'e'
    app 'gimp'
    app 'liferea'
    app 'worker'
    app 'mutt'
    app 'thunar'
    app 'yed'
    app 'rox'
    app 'ardour2'
    app 'rosegarden'
    app 'gentoo'
    app 'mypaint'
    app 'sqliteman'
    echo 'play'
    app 'inkscape'
    app 'keepassx'
    app 'gvim'          'svim'
    app 'geeqie'
    app 'gpicview'
    app 'epdfview'
    app 'qpdfview'
    app 'xpdf'
    app 'mirage'
    app 'vncviewer'
    app 'audacity'
    app 'znotes'
    app 'irssi'
    app 'rawtherapee'
    app 'deadbeef'
    app 'claws-mail'
    app 'claws-mail'    'compose'
    app 'gcalctool'
    app 'klavaro'
    app 'scilab'
    app 'djview'
    app 'filezilla'
    app 'qtcreateor'
    app 'designer-qt4'
    app 'darktable'
    echo 'calc'
    app 'pgadmin3'
    app 'gpa'
    app 'stellarium'
    app 'xfontsel'
    app 'gcolor2'
    app 'xxxterm'
    app 'openshot'
    app 'libreoffice'
    app 'avidemux2_gtk'
    app 'gnuplot'
    app 'mathomatic'
    app 'xterm'
    app 'gitk'
    app 'urxvt'
    app 'urxvtc'
    appoff 'transmission-daemon'
    appon 'transmission-daemon'  '!transmission-daemon'
    app 'transmission-qt'   'transmission'
    app 'pidgin'
    app 'xchat'
    app 'dia'
    app 'tmux'
    app 'vlc'
    app 'when'
    app 'openoffice.org-3.3.0'
    app 'nvidia-settings'
    app 'violet'
    app 'spass'

    appon 'skype'           '!skype'
    appoff 'skype'

    app 'VirtualBox'    'virtualbox'

    app 'nexuiz-glx'
    app 'warzone2100'
    app 'nexuiz'
    app 'vegastrike'
    app 'wireshark'

    app 'deadbeef'      'next'
    app 'deadbeef'      'previous'
    app 'deadbeef'      'pause'

    app 'setxkbmap'     'ru'
    app 'setxkbmap'     'ru (std)'
    app 'setxkbmap'     'lv'
    app 'setxkbmap'     'лж'
    app 'setxkbmap'     'дм'

    echo 'screenshot'
    echo 'area screenshot'

    echo 'config'

    app 'xscreensaver'  'screensaver'
    app 'xscreensaver'  'toggle screensaver'
    app 'AstoMenace'

    echo 'rmsong'

    echo 'fvwm console'
    echo 'fvwm log'

    if echo "$HOSTNAME" | grep "graudeejs" > /dev/null; then
        snd_unit="$(sysctl -n hw.snd.default_unit)"
        if [ "$snd_unit" = "4" ]; then
            echo "snd -> headphones"
        elif [ "$snd_unit" = "5" ]; then
            echo "snd -> speakers"
        else
            echo "snd -> headphones"
            echo "snd -> speakers"
        fi
    fi

    echo 'SHUTDOWN'
    echo 'REBOOT'
    echo 'SLEEP'
    echo '!!! KILL !!!'

} | $DMENU`


case $selection in
'' ) ;;
'!!! KILL !!!' )        exec $XDG_CONFIG_HOME/wmscripts/KILL.sh ;;
'REBOOT' )              exec /sbin/shutdown -r now ;;
'SHUTDOWN' )            exec /sbin/shutdown -p now ;;
'firefox profiles' )    exec firefox -ProfileManager -new-instance ;;
'area screenshot' )     exec $XDG_CONFIG_HOME/wmscripts/mkScreenshot.sh --area --show;;
'browser' )             exec firefox ;;
'calc' )                exec $UTERM -name bc -e bc -l ;;
'editor' )              exec ${GUI_EDITOR:-${UTERM:-xterm} -e ${EDITOR:-vi}} ;;
'terminal' )            exec ${UTERM:-xterm} ;;
'worker' )              exec worker "${WMSCRIPTS_EXEC_PWD:-$HOME}" ;;
'chrome (private)' )    exec chrome --incognito ;;
'chrome')               exec chrome $CHROME_CMDL ;;
'compose' )             exec claws-mail --compose ;;
'config' )              menu_config ;;
'mutt' )                exec $UTERM -name mutt -e neomutt -y ;;
'firefox' )             exec firefox;;
'fvwm console' )        exec FvwmCommand "Function Fvwm_Console" ;;
'fvwm log' )            exec $UTERM -name 'fvwm log' -e tail -f $HOME/.xsession-errors ;;
'gnuplot' )             exec $UTERM -name gnuplot -e gnuplot ;;
'irssi' )               exec $UTERM -name irssi -e irssi ;;
'lv' | 'лж' | 'дм' )    exec setxkbmap lv ;;
'mathomatic' )          exec $UTERM -name mathomatic -e mathomatic ;;
'next' )                exec deadbeef --next ;;
'libreoffice' )         exec libreoffice ;;
'tmux' )                exec $UTERM -name tmux -e tmux ;;
'pause' )               exec deadbeef --pause ;;
'previous' )            exec deadbeef --prev ;;
'ru (std)' )            exec setxkbmap ru ;;
'ru' )                  exec setxkbmap ru phonetic ;;
'scilab' )              exec $UTERM -name scilab -e scilab -nwni -nb ;;
'screensaver' )         exec screensaverCtl.sh lock ;;
'screenshot' )          exec mkScreenshot.sh show ;;
'term' )                exec $UTERM ;;
'toggle screensaver' )  exec screensaverCtl.sh toggle ;;
'gpa' )                 exec gpa -k ;;
'transmission')         if pgrep -a trayer stalonetray > /dev/null || [ $WM = 'wmfs' ]; then TR_FLAGS='-m'; else TR_FLAGS=''; fi; exec nice -n 20 transmission-qt $TR_FLAGS ;;
'transmission-daemon')  exec nice -n 20 transmission-daemon ;;
'svim' )                exec gvim --servername VIM ;;
'vim' )                 exec $UTERM -name vim -e vim ;;
'virtualbox' )          kldstat | grep vboxdrv > /dev/null || sudo kldload vboxdrv; exec VirtualBox ;;
'when' )                when --warp=0 --nopaging --monday_first --noampm | exec message.sh -name when -buttons "_OK" -default '_OK' -file - ;;
'spass' )               spass -s 'a-hjkmnp-zA-HJKMNP-Z2-9' -l 24 | xclip && exec message.sh -name spass -buttons '_OK' -default '_OK' 'password generated' ;;
'!skype' )              kill -s QUIT `pgrep skype`;;
'snd -> speakers' )     exec sudo /sbin/sysctl hw.snd.default_unit=4 ;;
'snd -> headphones' )   exec sudo /sbin/sysctl hw.snd.default_unit=5 ;;
'wtf '* )               exec message.sh "`wtf "$(echo "$selection" | sed 's/^wtf //')"`" ;;

'http://'* | 'https://'* ) open $selection ;;
*)                      exec $selection ;;
esac
# 1}}}

# vim: set ts=4 sw=4:
