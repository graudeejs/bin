#!/bin/sh

[ -f "$XDG_CONFIG_HOME/wmscripts/config" ] && . "$XDG_CONFIG_HOME/wmscripts/config"

# Some WM's (dwm and xmonad for example) need to execute some X commands
# themselves (Running from .xinitrc won't work). Put these commands here

# [ ! -f "$HOME/.laptop" ] && numlockx on || numlockx off

xset r rate 250 50 b off dpms $DPMS
xrdb "$HOME/.Xdefaults"
xsetroot -cursor_name left_ptr
swLang default
screensaverCtl.sh start
