#!/bin/sh

OS="${OS:-`uname`}"
case "$OS" in
"FreeBSD" )
  PG_DIR="/var/db/postgresql/data"
  # I'm using custom location
  service postgresql stop
  rm -Rf "${PG_DIR}.bak"
  mv "${PG_DIR}" "${PG_DIR}.bak"
  service postgresql initdb
  cp "${PG_DIR}.bak/pg_hba.conf"     "${PG_DIR}/pg_hba.conf"
  cp "${PG_DIR}.bak/postgresql.conf" "${PG_DIR}/postgresql.conf"
  service postgresql start
  if [ -n "$1" ]; then
    PGUSER="${PGUSER:-pgsql}" psql -d postgres -f "$1"
  else
    PGUSER="${PGUSER:-pgsql}" psql -d postgres < /dev/stdin
  fi
  ;;

* )
  echo "Not implemented" >> /dev/stderr
  exit 1
  ;;
esac
