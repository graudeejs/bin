#!/bin/sh

DATE="`date +%Y%m%d`"

ROUTERS="r0.lan r1.lan r2.lan"
BACKUP_DIR="$HOME/Documents/router/"
mkdir -p "$BACKUP_DIR"

if which gpg2 > /dev/null; then
    GPG=gpg2
else
    GPG=gpg
fi

for router_name in $ROUTERS; do
    ssh "${router_name}" "/system backup save dont-encrypt=yes name=${router_name}-${DATE}"

    backup_file="$BACKUP_DIR/${DATE}-${router_name}.backup"

    sftp "${router_name}:${router_name}-${DATE}.backup" $backup_file
    $GPG --batch --recipient aldis@berjoza.lv --encrypt "$backup_file"
    rm -f "$backup_file"
    sleep 1
    ssh "${router_name}" "/file remove ${router_name}-${DATE}.backup"
    echo "${router_name} backup saved"
done
