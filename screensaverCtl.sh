#!/bin/sh

# Script to control xscreensaver

[ -f "$XDG_CONFIG_HOME/wmscripts/config" ] && . "$XDG_CONFIG_HOME/wmscripts/config"

SCREENSAVER_APP=${SCREENSAVER_APP:-xautolock}
SCREENSAVER_NICE=${SCREENSAVER_NICE:-10}
SCREENSAVER_TIMEOUT=${SCREENSAVER_TIMEOUT:-5}
DPMS=${DPMS:-300 600 900}

PID=`pgrep $SCREENSAVER_APP`

start() { # {{{
	# return 0 if screensaver was started by function

	case $SCREENSAVER_APP in
	'xscreensaver' )
		if [ ! $PID ]; then
			nice -n $SCREENSAVER_NICE xscreensaver -no-splash &
			return 0
		fi
		;;

	'xautolock' )
		# don't start xautolock with nice, because xLocker.sh will launch xlock with nice
		if [ ! $PID ]; then
			xautolock -time $SCREENSAVER_TIMEOUT -locker xLocker.sh -nowlocker xLocker.sh &
			return 0
		fi
		;;

	esac
	return 1
} # }}}

stop() { # {{{
	case $SCREENSAVER_APP in
	'xscreensaver' )
		if [ $PID ]; then
			xscreensaver-command -exit > /dev/null
			return 0
		fi
		;;

	'xautolock' )
		if [ $PID ]; then
			xautolock -exit > /dev/null
			return 0
		fi
		;;

	esac
	return 1
} # }}}

toggle() { # {{{

	case $SCREENSAVER_APP in
	'xscreensaver' )
		if [ $PID ]; then
			stop
			return 1
		else
			start
			return 0
		fi
		;;

	'xautolock' )
		if [ $PID ]; then
			stop
			return 1
		else
			start
			return 0
		fi
		;;

	'xlock' )
		return 1
		;;
	esac

} # }}}

lock() { # {{{
	case $SCREENSAVER_APP in
	'xscreensaver' )
		[ $PID ] || start
		xscreensaver-command -lock > /dev/null
		;;

	'xautolock' )
		if [ ! $PID ]; then
			start
			sleep 3 # lock won't work without a good sleep after starting up. :)
					# not sure if 3s is enough for all machines, but it works
					# fine on my PC. In fact, it worked fine with 2s
		fi
		xautolock -locknow
		;;

	'xlock' )
		xlock -nice ${SCREENSAVER_NICE:-10} &
		;;
	esac

} # }}}


case $1 in
'status' )
	case $SCREENSAVER_APP in
	'xscreensaver' | 'xautolock' )
		echo $PID
		;;

	'xlock' )
		echo "$SCREENSAVER_APP doesn't support this" 2> /dev/stderr
		exit 1
		;;
	esac
	;;

'start' | '' )
	xset dpms $DPMS
	start
	;;

'toggle-movie-mode' )
	toggle && xset dpms $DPMS || xset dpms force on
	;;

'stop' )
	stop
	;;

'toggle' )
	toggle
	;;

'lock' )
	killall -s HUP gpg-agent
	xset dpms force off
	lock
	;;

'monitor-off' )
	killall -s HUP gpg-agent
	killall qtpass
	xset dpms force off
	;;

* )
	echo "Unsupported option" > /dev/stderr
	echo "Supported commands: status|start|stop|toggle-movie-mode|lock|monitor-off" > /dev/stderr
	exit 1
	;;
esac

exit

# vim: set ts=4 sw=4:
