#!/bin/sh

OPENSSL="${OPENSSL:-openssl}"

if [ $# -eq 3 ]; then
    ALIAS="$1"
    CERT="$2"
    CA=""
    KEY="$3"
else
    ALIAS="$1"
    CERT="$2"
    CA="$3"
    KEY="$4"
fi

FAIL=0

warn() {
    echo "$@" >> /dev/stderr
}

if [ "$CERT" = "" ]; then
    warn "Path to certificate missing"
    FAIL=1
fi

if [ "$CA" = "" ]; then
    warn "Path to CA missing"
fi

if [ "$KEY" = "" ]; then
    warn "Path to KEY missing"
    FAIL=1
fi

if [ $FAIL -ne 0 ]; then
    warn
    warn "USAGE:"
    warn "ssl_fx_build ALIAS CERT CA KEY"
    exit 1
fi

CERT_WITH_CA=$(mktemp)
if [ "$CA" = "" ]; then
    cp "$CERT" "$CERT_WITH_CA"
else
    cat "$CERT" "$CA" > "$CERT_WITH_CA"
fi

$OPENSSL pkcs12 -export -in "$CERT_WITH_CA" -inkey "$KEY" -name "$ALIAS"
rm -f "$CERT_WITH_CA"
