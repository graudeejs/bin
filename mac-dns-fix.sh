#!/bin/sh

LOCAL_DNS="192.168.0.1" # Space separated list of default DNS server IPs. Adjust to your home setup
INTERFACE='Wi-Fi'

list_dns() {
    networksetup -getdnsservers "$INTERFACE"
}

set_dns() {
    printf "    Set DNS: "
    echo "$@"
    sudo networksetup -setdnsservers "$INTERFACE" "$@"
}

printf "Current DNS: "
echo $(list_dns)

if [ "$1" != '' ]; then
    set_dns "$@"
    exit 0
fi

VPN_DNS=$(list_dns | grep -E '^10\.')

if [ "$VPN_DNS" = '' ]; then
    set_dns $LOCAL_DNS
else
    set_dns $VPN_DNS
fi
