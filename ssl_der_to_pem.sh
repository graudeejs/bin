#!/bin/sh
OPENSSL="${OPENSSL:-openssl}"
DER_FILE="$1"
PEM_FILE="${2:-"$DER_FILE.pem"}"
if [ "$DER_FILE" = '' ]; then
    echo "DER file no provided"
fi
$OPENSSL x509 -inform der -in "$DER_FILE" -outform pem -out "$PEM_FILE"
