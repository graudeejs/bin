#!/bin/sh
OPENSSL="${OPENSSL:-openssl}"
if [ "$1" = '' ]; then
    echo "path to key not provided"
    exit 1
fi
openssl rsa -aes256 -in "$1"
