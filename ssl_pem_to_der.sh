#!/bin/sh
OPENSSL="${OPENSSL:-openssl}"
PEM_FILE="$1"
DER_FILE="${2:-"$PEM_FILE.der"}"
if [ "$PEM_FILE" = '' ]; then
    echo "PEM file no provided"
fi
$OPENSSL x509 -inform pem -in "$PEM_FILE" -outform der -out "$DER_FILE"
