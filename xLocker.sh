#!/bin/sh

# This script is used by xautolock to lock screen

[ -f "$XDG_CONFIG_HOME/wmscripts/config" ] && . "$XDG_CONFIG_HOME/wmscripts/config"
killall -s HUP gpg-agent
swLang default
exec xlock +allowroot -nice ${SCREENSAVER_NICE:-10} -lockdelay 60 -timeout 60 -startCmd startScreensaverCMDS.sh -endCmd stopScreensaverCMDS.sh

