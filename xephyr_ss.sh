#!/bin/sh

XDISPLAY=${XDISPLAY:-1}
Xephyr -ac -br -noreset -screen 1900x1200 :$XDISPLAY &
XPID=$?
trap "kill $XPID" EXIT

export DISPLAY=:$XDISPLAY.0
hsetroot -fill $HOME/.config/wallpaper
${1:fvwm}
