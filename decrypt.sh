#!/bin/sh

for src in $@; do
    dest=$(echo "$src" | sed -E 's#\.(gpg|asc)$##')
    if [ ! -f "$dest" ]; then
        gpg2 --decrypt "$src" > "$dest"
    else
        echo "$dest exists. Skipping" > /dev/stderr
    fi
done
