#!/bin/sh

CURRENT_DIR="`pwd`"

while [ "`pwd`" != '/' ]; do
    for csv_dir in .git .svn .hg .csv .bzr; do
        if [ -d "$csv_dir" ]; then
            export PROJECT_DIR="`pwd`"
            break
        fi
    done
    [ "$PROJECT_DIR" = '' ] && cd .. || break
done

[ "$PROJECT_DIR" = '' ] && cd "$CURRENT_DIR"

PROJECT_NAME="`pwd | sed 's#^.*/##; s#[ \.]#_#g;' | tr '[a-z]' '[A-Z]'`"
cd "$CURRENT_DIR"

if [ `uname` = 'Darwin' ]; then
    VIM_NAME=mvim
elif [ "$DISPLAY" != '' ]; then
    case "`echo "$0" | sed 's#^.*/##'`" in
    'e'  ) VIM_NAME="gvim" ;;
    'ce' ) VIM_NAME="vim" ;;
    *    ) echo "Unsupported" > /dev/stderr; exit 1 ;;
    esac
else
    VIM_NAME="vim"
fi

if $VIM_NAME --serverlist | egrep "^$PROJECT_NAME$" > /dev/null 2> /dev/null; then
    REMOTE='--remote'
    if [ "$WM" = 'fvwm' ]; then
        FvwmCommand "Next (\"*$PROJECT_NAME\") MoveToDesk" \
            "Next (\"*$PROJECT_NAME\") Raise" \
            "Next (\"*$PROJECT_NAME\") Focus"
    fi
fi

exec $VIM_NAME --servername "$PROJECT_NAME" $REMOTE "$@"
