#!/bin/sh

# wraper script to ImageMagick and GraphicsMagick
# args - anything that works for ImageMagick or GraphicsMagic

if which import > /dev/null; then
	$*
	exit $?
elif which gm > /dev/null; then
	# GraphicsMagick has akwarded return codes
	gm $* && exit 1 || exit 0
fi
echo "ImageMagick and GraphicsMagic aren't installed" > /dev/stderr
exit 1
