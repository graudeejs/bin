BEGIN { FS = "=" }

function print_char(char)
{
  if (char == "%") {
    printf "%%"
  }
  else {
    printf char
  }
}

function print_sanitized(value)
{
  split(value, tmp, "")
  tmp_len = length(tmp)
  if (length(tmp[1])) print_char(tmp[1])
  for(i = 1; i <= tmp_len - 3; i++) print_char("*")
  for(i = tmp_len - 3; i <= tmp_len; i++) if (length(tmp[i]) != 0) print_char(tmp[i])
}

{ sanitize = 0 }

$1 ~ /(SECRET|KEY|PASS|TOKEN|AUTH|SIGNATURE)/ {
  sanitize = 1
}

sanitize == 1 {
  printf $1 "="
  print_sanitized($2)
  printf "\n"
}

sanitize == 0 {
  print $0
}
