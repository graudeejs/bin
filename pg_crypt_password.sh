#!/bin/sh

USERNAME="$1"
PASSWORD="$2"

OS=$(uname)

if [ "$USERNAME" = '' ]; then
    read -p 'username: ' USERNAME
fi
if [ "$PASSWORD" = '' ]; then
    read -p 'password: ' PASSWORD
fi


case "$OS" in
"FreeBSD")
    echo "md5$(md5 -s "${PASSWORD}${USERNAME}" | cut -w -f 4)"
    ;;
"Linux")
    echo "md5$(echo -n "${PASSWORD}${USERNAME}" | md5sum | cut -d ' ' -f 1)"
    ;;
*)
    echo "Not implemented on your os" > /dev/stderr
    exit 1
    ;;
esac
