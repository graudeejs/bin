#!/bin/sh

OPENSSL="${OPENSSL:-openssl}"
ROOT_CA="$1"
INTERMEDIATE_CA="$2"
CERT="$3"

FAIL=0

warn() {
    echo "$@" >> /dev/stderr
}

if [ "$CERT" = "" ]; then
    warn "Path to CERT missing"
    FAIL=1
fi

if [ "$ROOT_CA" = "" ]; then
    warn "Path to ROOT_CA missing"
    FAIL=1
fi

if [ "$INTERMEDIATE_CA" = "" ]; then
    warn "Path to INTERMEDIATE_CA missing"
    FAIL=1
fi

if [ $FAIL -ne 0 ]; then
    warn
    warn "USAGE:"
    warn "ssl_verify_chain.sh CA INTERMEDIATE_CA CERT"
    exit 1
fi

$OPENSSL verify -CAfile "$ROOT_CA" -untrusted "$INTERMEDIATE_CA" "$CERT"
