#!/bin/sh
OPENSSL="${OPENSSL:-openssl}"
$OPENSSL pkcs12 -in "$1" -nocerts -nodes | sed -ne '/-BEGIN PRIVATE KEY-/,/-END PRIVATE KEY-/p'
