#!/bin/sh
OPENSSL="${OPENSSL:-openssl}"
$OPENSSL pkcs12 -in "$1" -clcerts -nokeys | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p' | $OPENSSL x509 -noout -dates
