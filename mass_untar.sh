#!/bin/sh

INITIAL_PWD=$(pwd)
find . -name '*.tar' -or -name '*.tar.gz' -or -name '*.tar.bz2' -or -name '*.tar.xz' -or -name '*.tgz' -or -name '*.txz' -or -name '*.tbz' | while read FULL_PATH; do
    DIR=$(dirname "$FULL_PATH")
    FILE=$(echo "$FULL_PATH" | sed 's#.*/##')
    cd "$DIR"
    tar -xf "$FILE"
    cd "$INITIAL_PWD"
done
