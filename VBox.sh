#!/bin/sh

# Script to manage VirtualBox on FreeBSD (Needs more work)
# Arg1 - start|stop|status

[ "${OS:-`uname`}" = 'FreeBSD' ] || exit 1

case "$1" in
'start')
	kldstat | grep -q vboxdrv || {
		sudo kldload vboxdrv
#		sudo /usr/local/etc/rc.d/vboxnet onestart
	}
	;;

'stop')
	kldstat | grep -q vboxdrv && {
#		sudo /usr/local/etc/rc.d/vboxnet onestop
		sudo kldunload vboxdrv
	}
	;;

#'restart')
#	kldstat | grep -q vboxdrv && sudo /usr/local/etc/rc.d/vboxnet onerestart
#	;;

'status')
	kldstat | grep -q vboxdrv && echo 'vboxdrv loaded' || echo 'vboxdrv not loaded'
	;;

*)
	echo "`basename $0` [ start | stop | status | restart ]"
	;;

esac
