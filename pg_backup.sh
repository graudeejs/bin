#!/bin/sh

PGUSER="${PGUSER:-pgsql}" pg_dumpall > "pg_dump_`date '+%Y%m%d'`.sql"
