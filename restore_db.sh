#!/bin/sh

if [ "$1" = '' -o "$2" = '' ]; then
    echo "Usage:" > /dev/stderr
    echo "$0 db_name dump_file" > /dev/stderr
    exit 1
fi

DISABLE_DATABASE_ENVIRONMENT_CHECK=1 rake db:drop RAILS_ENV=development
rake db:create RAILS_ENV=development

case "$2" in
*.gpg | *.asc)
    gpg2 --decrypt "$2" | pg_restore --verbose --clean --no-acl --no-owner -d "$1"
    RESTORE_STATUS=$?
    ;;
*)
    pg_restore --verbose --clean --no-acl --no-owner -d "$1" "$2"
    RESTORE_STATUS=$?
    ;;
esac

if [ $RESTORE_STATUS -eq 0 ]; then
    echo "something went wrong" > /dev/stderr
    exit 1
fi

rake db:migrate RAILS_ENV=development

rake db:drop RAILS_ENV=test > /dev/null 2> /dev/null
rake db:create db:migrate db:seed RAILS_ENV=test > /dev/null 2> /dev/null

case "$1" in
'cakehr_dev'* )
    echo 'CompanyUser.update_all(picture_file_name: nil); Redis.new.flushall' | bundle exec rails c
    ;;
esac
