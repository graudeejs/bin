#!/bin/sh
OPENSSL="${OPENSSL:-openssl}"
$OPENSSL pkcs12 -in "$1" -cacerts -nokeys -chain | sed -ne '/-BEGIN CERTIFICATE-/,/-END CERTIFICATE-/p'
