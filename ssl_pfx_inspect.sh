#!/bin/sh -e
OPENSSL="${OPENSSL:-openssl}"
$OPENSSL pkcs12 -in "$1" -clcerts -nokeys | $OPENSSL x509 | $OPENSSL x509 -noout -text
