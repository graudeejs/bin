#!/bin/sh
OPENSSL="${OPENSSL:-openssl}"
if [ "$1" = '' ]; then
    echo "path to certificate not provided"
    exit 1
else
    $OPENSSL x509 -noout -text -in "$1"
fi
